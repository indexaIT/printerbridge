﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrinterBridge.Windows
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
        }

        private void m_invoices_Click(object sender, EventArgs e)
        {
            Bridge bridge = new Bridge();
            bridge.Show();
        }

        private void m_devolutions_Click(object sender, EventArgs e)
        {
            DevolutionsBridge devolutionsBridge = new DevolutionsBridge();
            devolutionsBridge.Show();
        }
    }
}
