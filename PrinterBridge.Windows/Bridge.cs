﻿using ClassLibrary2;
using Npgsql;
using PrinterBridge.Library.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrinterBridge.Windows
{
    public partial class Bridge : Form
    {
        PowerShoesContext db;
        public Bridge()
        {
            InitializeComponent();
            db = new PowerShoesContext();
        }

        private void m_search_Enter(object sender, EventArgs e)
        {
            m_search.Text = "";
            m_search.ForeColor = Color.Black;
        }

        private void m_search_Leave(object sender, EventArgs e)
        {
            if (m_search.Text == "")
            {
                m_search.Text = "Buscar...";
                m_search.ForeColor = Color.Silver;
            }
        }

        public void FillInvoiceList()
        {
            string searchText = m_search.Text == "Buscar..." ? "":m_search.Text;
            facturaEncabezadoBindingSource.DataSource = (from f in db.FacturaEncabezado
                                                         where f.id_factura.ToString().Contains(searchText) || searchText == ""
                                                         select f).OrderByDescending(x => x.id_factura).Take(1000).ToList();
        }

        private void Bridge_Load(object sender, EventArgs e)
        {
            FillInvoiceList();
            m_print.BackColor = Color.Silver;
            m_invoicesList.SelectedRows[0].Selected = false;
        }
        

        private void m_refreshInvoices_Click(object sender, EventArgs e)
        {
            FillInvoiceList();
        }

        

        private void m_print_Click(object sender, EventArgs e)
        {
            int idFactura = Convert.ToInt32(m_invoicesList.SelectedRows[0].Cells[0].Value);
            List<FacturaEncabezado> header;
            List<FacturaDetalle> detail;
            List<CobrosFacturas> payment;
            List<Clientes> customer;
            if (db.Database.Connection.State != ConnectionState.Open)
                db.Database.Connection.Open();
            using (var connection = db.Database.Connection.BeginTransaction())
            {
                try
                {
                    header = (from h in db.FacturaEncabezado
                              where h.id_factura == idFactura
                              select h).ToList();
                    detail = (from d in db.FacturaDetalle
                              where d.id_factura == idFactura
                              select d).ToList();
                    payment = (from c in db.CobrosFacturas
                               where c.Id_Factura == idFactura
                               select c).ToList();
                    decimal? idCliente;
                    idCliente = (from f in db.FacturaEncabezado
                                 where f.id_factura == idFactura
                                 select f.id_cliente).FirstOrDefault();

                    customer = (from c in db.Clientes
                                where c.Id == idCliente
                                select c).ToList();
                    connection.Commit();
                }
                catch (Exception ex)
                {
                    connection.Rollback();
                    throw ex;
                }
            }
            if (db.Database.Connection.State == ConnectionState.Open)
            {
                db.Database.Connection.Close();
            }


            DataTable headerTable, detailTable, paymentTable, customerTable = new DataTable();

            headerTable = ConvertToDataTable.ToDataTable(header);
            detailTable = ConvertToDataTable.ToDataTable(detail);
            paymentTable = ConvertToDataTable.ToDataTable(payment);
            customerTable = ConvertToDataTable.ToDataTable(customer);
            FiscalDLL fiscal = new FiscalDLL();
            fiscal.ProcessCPSQL(true, false, headerTable, detailTable, paymentTable, customerTable, null);
        }

        private void m_invoicesList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void m_back_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void m_search_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                FillInvoiceList();
            }
        }

        private void m_invoicesList_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            m_print.BackColor = Color.Green;
            m_print.Enabled = true;
        }
    }
}
