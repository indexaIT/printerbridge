﻿using System;
using System.Windows.Forms;

namespace PrinterBridge.Windows
{
    partial class DevolutionsBridge
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.m_print = new System.Windows.Forms.Button();
            this.m_devolutionsList = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Id_Factura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.montototalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.devolucionEncabezadoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.m_back = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.m_search = new System.Windows.Forms.TextBox();
            this.m_refreshInvoices = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.m_devolutionsList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.devolucionEncabezadoBindingSource)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_print
            // 
            this.m_print.BackColor = System.Drawing.SystemColors.ControlDark;
            this.m_print.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.m_print.Enabled = false;
            this.m_print.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_print.Location = new System.Drawing.Point(0, 557);
            this.m_print.Name = "m_print";
            this.m_print.Size = new System.Drawing.Size(384, 104);
            this.m_print.TabIndex = 1;
            this.m_print.Text = "IMPRIMIR";
            this.m_print.UseVisualStyleBackColor = false;
            this.m_print.Click += new System.EventHandler(this.m_print_Click);
            // 
            // m_devolutionsList
            // 
            this.m_devolutionsList.AllowUserToAddRows = false;
            this.m_devolutionsList.AllowUserToDeleteRows = false;
            this.m_devolutionsList.AutoGenerateColumns = false;
            this.m_devolutionsList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_devolutionsList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Id_Factura,
            this.montototalDataGridViewTextBoxColumn});
            this.m_devolutionsList.DataSource = this.devolucionEncabezadoBindingSource;
            this.m_devolutionsList.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.m_devolutionsList.Location = new System.Drawing.Point(0, 198);
            this.m_devolutionsList.Name = "m_devolutionsList";
            this.m_devolutionsList.ReadOnly = true;
            this.m_devolutionsList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.m_devolutionsList.Size = new System.Drawing.Size(384, 359);
            this.m_devolutionsList.TabIndex = 2;
            this.m_devolutionsList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.m_devolutionsList_CellContentClick);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Número Devolución";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            // 
            // Id_Factura
            // 
            this.Id_Factura.DataPropertyName = "Id_Factura";
            this.Id_Factura.HeaderText = "Número Factura";
            this.Id_Factura.Name = "Id_Factura";
            this.Id_Factura.ReadOnly = true;
            // 
            // montototalDataGridViewTextBoxColumn
            // 
            this.montototalDataGridViewTextBoxColumn.DataPropertyName = "montototal";
            this.montototalDataGridViewTextBoxColumn.HeaderText = "Monto Total";
            this.montototalDataGridViewTextBoxColumn.Name = "montototalDataGridViewTextBoxColumn";
            this.montototalDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // devolucionEncabezadoBindingSource
            // 
            this.devolucionEncabezadoBindingSource.DataSource = typeof(PrinterBridge.Library.Models.DevolucionEncabezado);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.panel1.Controls.Add(this.m_back);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.m_search);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(384, 75);
            this.panel1.TabIndex = 3;
            // 
            // m_back
            // 
            this.m_back.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.m_back.Dock = System.Windows.Forms.DockStyle.Right;
            this.m_back.ForeColor = System.Drawing.Color.Navy;
            this.m_back.Location = new System.Drawing.Point(309, 0);
            this.m_back.Name = "m_back";
            this.m_back.Size = new System.Drawing.Size(75, 49);
            this.m_back.TabIndex = 7;
            this.m_back.Text = "Volver";
            this.m_back.UseVisualStyleBackColor = false;
            this.m_back.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(307, 29);
            this.label1.TabIndex = 6;
            this.label1.Text = "Devoluciones para Imprimir";
            // 
            // m_search
            // 
            this.m_search.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.m_search.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_search.ForeColor = System.Drawing.Color.Silver;
            this.m_search.Location = new System.Drawing.Point(0, 49);
            this.m_search.Name = "m_search";
            this.m_search.Size = new System.Drawing.Size(384, 26);
            this.m_search.TabIndex = 5;
            this.m_search.Text = "Buscar...";
            this.m_search.TextChanged += new System.EventHandler(this.m_search_TextChanged);
            this.m_search.Enter += new System.EventHandler(this.m_search_Enter);
            this.m_search.Leave += new System.EventHandler(this.m_search_Leave);
            // 
            // m_refreshInvoices
            // 
            this.m_refreshInvoices.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.m_refreshInvoices.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_refreshInvoices.Location = new System.Drawing.Point(0, 81);
            this.m_refreshInvoices.Name = "m_refreshInvoices";
            this.m_refreshInvoices.Size = new System.Drawing.Size(384, 117);
            this.m_refreshInvoices.TabIndex = 5;
            this.m_refreshInvoices.Text = "Refrescar listado";
            this.m_refreshInvoices.UseVisualStyleBackColor = true;
            this.m_refreshInvoices.Click += new System.EventHandler(this.m_refreshDevolutions_Click);
            // 
            // DevolutionsBridge
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 661);
            this.Controls.Add(this.m_refreshInvoices);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.m_devolutionsList);
            this.Controls.Add(this.m_print);
            this.MaximizeBox = false;
            this.Name = "DevolutionsBridge";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Printer Bridge 1.0 - Devoluciones";
            this.Load += new System.EventHandler(this.DevolutionsBridge_Load);
            ((System.ComponentModel.ISupportInitialize)(this.m_devolutionsList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.devolucionEncabezadoBindingSource)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        






        #endregion
        private System.Windows.Forms.Button m_print;
        private System.Windows.Forms.DataGridView m_devolutionsList;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox m_search;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button m_back;
        private System.Windows.Forms.Button m_refreshInvoices;
        private DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn idfacturaDataGridViewTextBoxColumn;
        private BindingSource devolucionEncabezadoBindingSource;
        private DataGridViewTextBoxColumn Id;
        private DataGridViewTextBoxColumn Id_Factura;
        private DataGridViewTextBoxColumn montototalDataGridViewTextBoxColumn;
    }
}

