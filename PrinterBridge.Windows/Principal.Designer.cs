﻿namespace PrinterBridge.Windows
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_invoices = new System.Windows.Forms.Button();
            this.m_devolutions = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_invoices
            // 
            this.m_invoices.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.m_invoices.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_invoices.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_invoices.Location = new System.Drawing.Point(0, 0);
            this.m_invoices.Name = "m_invoices";
            this.m_invoices.Size = new System.Drawing.Size(284, 130);
            this.m_invoices.TabIndex = 0;
            this.m_invoices.Text = "Facturas";
            this.m_invoices.UseVisualStyleBackColor = false;
            this.m_invoices.Click += new System.EventHandler(this.m_invoices_Click);
            // 
            // m_devolutions
            // 
            this.m_devolutions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.m_devolutions.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_devolutions.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_devolutions.Location = new System.Drawing.Point(0, 130);
            this.m_devolutions.Name = "m_devolutions";
            this.m_devolutions.Size = new System.Drawing.Size(284, 130);
            this.m_devolutions.TabIndex = 1;
            this.m_devolutions.Text = "Devoluciones";
            this.m_devolutions.UseVisualStyleBackColor = false;
            this.m_devolutions.Click += new System.EventHandler(this.m_devolutions_Click);
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.m_devolutions);
            this.Controls.Add(this.m_invoices);
            this.Name = "Principal";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Printer Bridge 1.0 - Principal";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button m_invoices;
        private System.Windows.Forms.Button m_devolutions;
    }
}