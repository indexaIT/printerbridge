﻿using ClassLibrary2;
using PrinterBridge.Library.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrinterBridge.Windows
{
    public partial class DevolutionsBridge : Form
    {
        PowerShoesContext db;
        public DevolutionsBridge()
        {
            InitializeComponent();
            db = new PowerShoesContext();
        }
        private void m_search_Enter(object sender, EventArgs e)
        {
            m_search.Text = "";
            m_search.ForeColor = Color.Black;
        }

        private void m_search_Leave(object sender, EventArgs e)
        {
            if (m_search.Text == "")
            {
                m_search.Text = "Buscar...";
                m_search.ForeColor = Color.Silver;
            }
        }
        public void FillDevolutionList()
        {
            devolucionEncabezadoBindingSource.DataSource = db.DevolucionEncabezado.Where(f => f.Id.ToString().Contains(m_search.Text) || m_search.Text == "Buscar...").OrderByDescending(x => x.Id).ToList();
        }

        private void DevolutionsBridge_Load(object sender, EventArgs e)
        {
            FillDevolutionList();
            m_print.BackColor = Color.Silver;
        }

        private void m_search_TextChanged(object sender, EventArgs e)
        {
            FillDevolutionList();
        }

        private void m_refreshDevolutions_Click(object sender, EventArgs e)
        {
            FillDevolutionList();
        }
        public DataTable LINQToDataTable<T>(IEnumerable<T> varlist)
        {
            DataTable dtReturn = new DataTable();

            // column names 
            PropertyInfo[] oProps = null;

            if (varlist == null) return dtReturn;

            foreach (T rec in varlist)
            {
                // Use reflection to get property names, to create table, Only first time, others 
                //will follow
                 if (oProps == null)
                {
                    oProps = ((Type)rec.GetType()).GetProperties();
                    foreach (PropertyInfo pi in oProps)
                    {
                        Type colType = pi.PropertyType;

                        if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition()
                        == typeof(Nullable<>)))
                        {
                            colType = colType.GetGenericArguments()[0];
                        }

                        dtReturn.Columns.Add(new DataColumn(pi.Name, colType));
                    }
                }

                DataRow dr = dtReturn.NewRow();

                foreach (PropertyInfo pi in oProps)
                {
                    dr[pi.Name] = pi.GetValue(rec, null) == null ? DBNull.Value : pi.GetValue
                    (rec, null);
                }

                dtReturn.Rows.Add(dr);
            }
            return dtReturn;
        }
        

        private void m_print_Click(object sender, EventArgs e)
        {
            int idDevolution = System.Convert.ToInt32(m_devolutionsList.SelectedRows[0].Cells[0].Value);
            FiscalDLL fiscal = new FiscalDLL();

            var header = db.DevolucionEncabezado.Where(d => d.Id == idDevolution).Select(x => new
            {
                x.Id_Factura,
                x.cliente,
                x.rif,
                x.direccion,
                x.Id_Caja               
            });

            DataTable headerTable = LINQToDataTable(header);            

            var details = db.DevolucionDetalle.Where(d => d.id_devolucion == idDevolution).Select(x => new
            {
                x.Producto.Modelo,
                x.pvp,
                x.cantidad,
                x.DevolucionEncabezado.montototal
            }).ToList();

            DataTable detailsTable = LINQToDataTable(details);

            var payments = db.CobrosFacturas.Where(c => c.Id_Factura == header.FirstOrDefault().Id_Factura).Select(x => new
            {
                x.Id_FormaCobro,
                x.MontoCobro
            }).ToList();

            DataTable paymentsTable = LINQToDataTable(payments);

            var customer = db.DevolucionEncabezado.Where(d => d.Id == idDevolution).Select(x => new
            {
                x.cliente,
                x.rif,
                x.direccion
            });

            DataTable customerTable = LINQToDataTable(customer);

            fiscal.ProcessCPSQL(false, true,headerTable,detailsTable,paymentsTable,customerTable,new DataTable());
        }

        private void m_devolutionsList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            m_print.BackColor = Color.Green;
            m_print.Enabled = true;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
