﻿namespace PrinterBridge.Windows
{
    partial class Bridge
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.m_print = new System.Windows.Forms.Button();
            this.m_invoicesList = new System.Windows.Forms.DataGridView();
            this.id_factura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.montototalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.facturaEncabezadoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.m_back = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.m_search = new System.Windows.Forms.TextBox();
            this.m_refreshInvoices = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.m_invoicesList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.facturaEncabezadoBindingSource)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_print
            // 
            this.m_print.BackColor = System.Drawing.SystemColors.ControlDark;
            this.m_print.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.m_print.Enabled = false;
            this.m_print.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_print.Location = new System.Drawing.Point(0, 557);
            this.m_print.Name = "m_print";
            this.m_print.Size = new System.Drawing.Size(384, 104);
            this.m_print.TabIndex = 1;
            this.m_print.Text = "IMPRIMIR";
            this.m_print.UseVisualStyleBackColor = false;
            this.m_print.Click += new System.EventHandler(this.m_print_Click);
            // 
            // m_invoicesList
            // 
            this.m_invoicesList.AllowUserToAddRows = false;
            this.m_invoicesList.AllowUserToDeleteRows = false;
            this.m_invoicesList.AutoGenerateColumns = false;
            this.m_invoicesList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_invoicesList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_factura,
            this.montototalDataGridViewTextBoxColumn});
            this.m_invoicesList.DataSource = this.facturaEncabezadoBindingSource;
            this.m_invoicesList.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.m_invoicesList.Location = new System.Drawing.Point(0, 198);
            this.m_invoicesList.MultiSelect = false;
            this.m_invoicesList.Name = "m_invoicesList";
            this.m_invoicesList.ReadOnly = true;
            this.m_invoicesList.RowHeadersVisible = false;
            this.m_invoicesList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.m_invoicesList.Size = new System.Drawing.Size(384, 359);
            this.m_invoicesList.TabIndex = 2;
            this.m_invoicesList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.m_invoicesList_CellContentClick);
            this.m_invoicesList.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.m_invoicesList_CellMouseClick);
            // 
            // id_factura
            // 
            this.id_factura.DataPropertyName = "id_factura";
            this.id_factura.HeaderText = "Factura N.";
            this.id_factura.Name = "id_factura";
            this.id_factura.ReadOnly = true;
            // 
            // montototalDataGridViewTextBoxColumn
            // 
            this.montototalDataGridViewTextBoxColumn.DataPropertyName = "montototal";
            this.montototalDataGridViewTextBoxColumn.HeaderText = "Monto Total";
            this.montototalDataGridViewTextBoxColumn.Name = "montototalDataGridViewTextBoxColumn";
            this.montototalDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // facturaEncabezadoBindingSource
            // 
            this.facturaEncabezadoBindingSource.DataSource = typeof(PrinterBridge.Library.Models.FacturaEncabezado);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.panel1.Controls.Add(this.m_back);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.m_search);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(384, 75);
            this.panel1.TabIndex = 3;
            // 
            // m_back
            // 
            this.m_back.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.m_back.Dock = System.Windows.Forms.DockStyle.Right;
            this.m_back.ForeColor = System.Drawing.Color.Navy;
            this.m_back.Location = new System.Drawing.Point(309, 0);
            this.m_back.Name = "m_back";
            this.m_back.Size = new System.Drawing.Size(75, 49);
            this.m_back.TabIndex = 7;
            this.m_back.Text = "Volver";
            this.m_back.UseVisualStyleBackColor = false;
            this.m_back.Click += new System.EventHandler(this.m_back_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(253, 29);
            this.label1.TabIndex = 6;
            this.label1.Text = "Facturas para Imprimir";
            // 
            // m_search
            // 
            this.m_search.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.m_search.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_search.ForeColor = System.Drawing.Color.Silver;
            this.m_search.Location = new System.Drawing.Point(0, 49);
            this.m_search.Name = "m_search";
            this.m_search.Size = new System.Drawing.Size(384, 26);
            this.m_search.TabIndex = 5;
            this.m_search.Text = "Buscar...";
            this.m_search.Enter += new System.EventHandler(this.m_search_Enter);
            this.m_search.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_search_KeyDown);
            this.m_search.Leave += new System.EventHandler(this.m_search_Leave);
            // 
            // m_refreshInvoices
            // 
            this.m_refreshInvoices.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.m_refreshInvoices.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_refreshInvoices.Location = new System.Drawing.Point(0, 81);
            this.m_refreshInvoices.Name = "m_refreshInvoices";
            this.m_refreshInvoices.Size = new System.Drawing.Size(384, 117);
            this.m_refreshInvoices.TabIndex = 5;
            this.m_refreshInvoices.Text = "Refrescar listado";
            this.m_refreshInvoices.UseVisualStyleBackColor = true;
            this.m_refreshInvoices.Click += new System.EventHandler(this.m_refreshInvoices_Click);
            // 
            // Bridge
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 661);
            this.Controls.Add(this.m_refreshInvoices);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.m_invoicesList);
            this.Controls.Add(this.m_print);
            this.MaximizeBox = false;
            this.Name = "Bridge";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Printer Bridge 1.0 - Facturas";
            this.Load += new System.EventHandler(this.Bridge_Load);
            ((System.ComponentModel.ISupportInitialize)(this.m_invoicesList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.facturaEncabezadoBindingSource)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button m_print;
        private System.Windows.Forms.DataGridView m_invoicesList;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox m_search;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource facturaEncabezadoBindingSource;
        private System.Windows.Forms.Button m_back;
        private System.Windows.Forms.Button m_refreshInvoices;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_factura;
        private System.Windows.Forms.DataGridViewTextBoxColumn montototalDataGridViewTextBoxColumn;
    }
}

