﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterBridge.Library.Models
{
    [Table("clientes",Schema = "postgres")]
    public class Clientes
    {
        public Clientes()
        {
            FacturaEncabezados = new HashSet<FacturaEncabezado>();
            DevolucionEncabezados = new HashSet<DevolucionEncabezado>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity), Column("id_cliente")]
        public int Id { get; set; }
        [Column("cliente")]
        public string Cliente { get; set; }
        [Key]
        [Column("rif")]
        public string Rif { get; set; }
        [Column("telefono")]
        public string Telefono { get; set; }
        [Column("direccion")]
        public string Direccion { get; set; }
        [Column("descuento")]
        public decimal Descuento { get; set; }
        [Column("iva")]
        public char Iva { get; set; }
        [Column("contribuyente")]
        public char Contribuyente { get; set; }
        [Column("credito")]
        public char Credito { get; set; }
        [Column("comision")]
        public char comision { get; set; }
        [Column("ultimacompra")]
        public DateTime? UltimaCompra { get; set; }
        [Column("referencia1")]
        public string Referencia1 { get; set; }
        [Column("referencia2")]
        public string Referencia2 { get; set; }
        [Column("referencia3")]
        public string Referencia3 { get; set; }
        [Column("referencia4")]
        public string Referencia4 { get; set; }
        [Column("fecha_nac")]
        public DateTime? FechaNacimiento { get; set; }
        [Column("email")]
        public string Email { get; set; }
        [Column("activo")]
        public char Activo { get; set; }
        public string usr_crea { get; set; }
        public DateTime? fec_crea { get; set; }
        public string usr_mod { get; set; }
        public DateTime? fec_mod { get; set; }
        public char enviado { get; set; }

        public virtual ICollection<FacturaEncabezado> FacturaEncabezados { get; set; }
        public virtual ICollection<DevolucionEncabezado> DevolucionEncabezados { get; set; }
    }
}
