﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterBridge.Library.Models
{
    [Table("facturaencabezado",Schema = "postgres")]
    public class FacturaEncabezado
    {
        public FacturaEncabezado()
        {
            facturadetalle = new HashSet<FacturaDetalle>();
        }

        [Key, Column(Order = 0)]
        public int id_factura { get; set; }
        [Key]
        [Column("id_tienda", Order = 1)]
        public int id_tienda { get; set; }
        public int id_caja { get; set; }
        public int id_cajero { get; set; }
        public int id_vendedor { get; set; }
        public int id_cliente { get; set; }
        public string rif { get; set; }
        public string cliente { get; set; }
        public string direccion { get; set; }
        public string telefono { get; set; }
        public DateTime? fecha { get; set; }
        public decimal montototal { get; set; }
        public char credito { get; set; }
        public TimeSpan horaimpresion { get; set; }
        public string maquinafiscal { get; set; }        
        public int? id_factura_fiscal { get; set; }
        public string serie { get; set; }
        public int? id_moneda { get; set; }
        public decimal? tasa_cambio { get; set; }
        public string usr_crea { get; set; }
        public DateTime? fec_crea { get; set; }
        public string usr_mod { get; set; }
        public DateTime? fec_mod { get; set; }
        public char enviado { get; set; }

        public virtual Clientes Cliente { get; set; }
        public virtual ICollection<FacturaDetalle> facturadetalle { get; set; }

        
    }
}
