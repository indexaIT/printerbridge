﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterBridge.Library.Models
{
    [Table("productos",Schema = "postgres")]
    public class Productos
    {
        public Productos()
        {
            DevolucionDetalles = new HashSet<DevolucionDetalle>();
            FacturaDetalles = new HashSet<FacturaDetalle>();
        }

        [Key,Column("id_producto")]
        public int Id_Producto { get; set; }
        [Column("id_proveedor")]
        public int Id_Proveedor { get; set; }
        [Column("id_marca")]
        public int Id_Marca { get; set; }
        [Column("modelo")]
        public string Modelo { get; set; }
        [Column("id_material")]
        public int Id_Material { get; set; }
        [Column("id_linea")]
        public int Id_Linea { get; set; }
        [Column("id_impuesto")]
        public int Id_Impuesto { get; set; }
        [Column("descuento")]
        public char Descuento { get; set; }
        [Column("comision")]
        public char Comision { get; set; }
        [Column("tiempodespacho")]
        public decimal TiempoDespacho { get; set; }
        [Column("activo")]
        public char Activo { get; set; }
        [Column("ruta_foto")]
        public string RutaFoto { get; set; }
        [Column("usr_crea")]
        public string Usr_crea { get; set; }
        [Column("fec_crea")]
        public DateTime? Fec_crea { get; set; }
        [Column("usr_mod")]
        public string Usr_mod { get; set; }
        [Column("fec_mod")]
        public DateTime? Fec_mod { get; set; }
        [Column("enviado")]
        public char Enviado { get; set; }

        public virtual ICollection<DevolucionDetalle> DevolucionDetalles { get; set; }
        public virtual ICollection<FacturaDetalle> FacturaDetalles { get; set; }

    }
}
