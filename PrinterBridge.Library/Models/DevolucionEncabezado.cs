﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterBridge.Library.Models
{
    [Table("devolucionencabezado", Schema = "postgres")]
    public class DevolucionEncabezado
    {
        public DevolucionEncabezado()
        {
            devoluciondetalle = new HashSet<DevolucionDetalle>();
        }

        [Key]
        [Column("id_devolucion", Order = 0)]
        public int Id { get; set; }
        [Key]
        [Column("id_tienda", Order = 1)]
        public int Id_Tienda { get; set; }
        [Column("id_factura")]
        public int Id_Factura { get; set; }
        [Column("id_caja")]
        public int Id_Caja { get; set; }
        [Column("id_cajero")]        
        public int Id_Cajero { get; set; }
        [Column("id_vendedor")]
        public int Id_Vendedor { get; set; }
        public string rif { get; set; }
        public string cliente { get; set; }
        public string direccion { get; set; }
        public DateTime? fecha { get; set; }
        public decimal montototal { get; set; }
        public TimeSpan horaimpresion { get; set; }
        public string maquinafiscal { get; set; }
        public string maquinafiscal_devolucion { get; set; }
        public int? id_factura_fiscal { get; set; }
        public int? id_devolucion_fiscal { get; set; }
        public string serie { get; set; }
        public string control { get; set; }
        public int? id_moneda { get; set; }
        public decimal? tasa_cambio { get; set; }
        public string usr_crea { get; set; }
        public DateTime? fec_crea { get; set; }
        public string usr_mod { get; set; }
        public DateTime? fec_mod { get; set; }
        public char enviado { get; set; }
        
        [ForeignKey("Id_Vendedor")]
        public virtual Empleados Cajero { get; set; }
        public virtual ICollection<DevolucionDetalle> devoluciondetalle { get; set; }
    }
}
