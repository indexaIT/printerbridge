﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterBridge.Library.Models
{
    public class PowerShoesContext : DbContext
    {
        public PowerShoesContext() 
            : base(nameOrConnectionString: "PowerShoesConnection")
        {
        }

        public DbSet<FacturaEncabezado> FacturaEncabezado { get; set; }
        public DbSet<FacturaDetalle> FacturaDetalle { get; set; }
        public DbSet<DevolucionEncabezado> DevolucionEncabezado { get; set; }
        public DbSet<DevolucionDetalle> DevolucionDetalle { get; set; }
        public DbSet<Clientes> Clientes { get; set; }
        public DbSet<CobrosFacturas> CobrosFacturas { get; set; }
        public DbSet<Empleados> Empleados { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<PowerShoesContext>(null);

            modelBuilder.Entity<FacturaDetalle>()
                .HasKey(f => new { f.id_factura, f.id_tienda, f.id_producto, f.id_color, f.numero });

            modelBuilder.Entity<FacturaEncabezado>()
                .HasKey(f => new { f.id_factura, f.id_tienda });

            modelBuilder.Entity<DevolucionDetalle>()
                .HasKey(f => new { f.id_devolucion, f.id_tienda, f.id_producto, f.id_color, f.numero });

            modelBuilder.Entity<DevolucionEncabezado>()
                .HasKey(f => new { f.Id, f.Id_Tienda });

            modelBuilder.Conventions
            .Remove<StoreGeneratedIdentityKeyConvention>();
        }

        
    }
}
