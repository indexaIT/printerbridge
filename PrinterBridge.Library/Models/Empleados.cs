﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterBridge.Library.Models
{
    [Table("empleados", Schema = "postgres")]
    public class Empleados
    {
        [Key, Column("id_empleado")]
        public int Id_Empleado { get; set; }
        [Column("ci")]
        public string CI { get; set; }
        [Column("apellidos")]
        public string Apellidos { get; set; }
        [Column("nombres")]
        public string Nombres { get; set; }
        [Column("sexo")]
        public char Sexo { get; set; }
        [Column("direccion")]
        public string Direccion { get; set; }
        [Column("telefono")]
        public string Telefono { get; set; }
        [Column("nacionalidad")]
        public string Nacionalidad { get; set; }
        [Column("fechanacimiento")]
        public DateTime? FechaNacimiento { get; set; }
        [Column("cedula")]
        public string Cedula { get; set; }
        [Column("usuario")]
        public string Usuario { get; set; }
        [Column("clave")]
        public string Clave { get; set; }
        [Column("nivelacceso")]
        public int NivelAcceso { get; set; }
        [Column("foto")]
        public string Foto { get; set; }
        [Column("fecha_ingreso")]
        public DateTime? FechaIngreso { get; set; }
        [Column("fecha_egreso")]
        public DateTime? FechaEgreso { get; set; }
        [Column("motivo_egreso")]
        public string MotivoEgreso { get; set; }
        [Column("sueldo")]
        public decimal Sueldo { get; set; }
        [Column("cargo")]
        public int Cargo { get; set; }
        [Column("porc_retencion")]
        public decimal PorcRetencion { get; set; }
        [Column("rif")]
        public string Rif { get; set; }
        [Column("ivas")]
        public char Ivas { get; set; }
        [Column("contratado")]
        public char Contratado { get; set; }
        [Column("activo")]
        public char Activo { get; set; }
        [Column("usr_crea")]
        public string Usr_crea { get; set; }
        [Column("fec_crea")]
        public DateTime? Fec_crea { get; set; }
        [Column("usr_mod")]
        public string Usr_mod { get; set; }
        [Column("fec_mod")]
        public DateTime? Fec_mod { get; set; }
        [Column("enviado")]
        public char Enviado { get; set; }
    }
}
