﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterBridge.Library.Models
{
    [Table("devoluciondetalle", Schema = "postgres")]
    public class DevolucionDetalle
    {
        [Key, Column(Order = 0)]
        public int id_devolucion { get; set; }
        [Key, Column(Order = 1)]
        public int id_tienda { get; set; }
        [Key, Column(Order = 2)]
        public int id_producto { get; set; }
        [Key, Column(Order = 3)]
        public int id_color { get; set; }
        [Key, Column(Order = 4)]
        public decimal numero { get; set; }
        public int cantidad { get; set; }
        public decimal costo { get; set; }
        public decimal pvp { get; set; }
        public decimal pvptienda { get; set; }
        public decimal tpvptienda { get; set; }
        public decimal descuento { get; set; }
        public decimal impuesto { get; set; }
        public decimal totalproducto { get; set; }
        public int? id_impuesto { get; set; }
        public decimal? tasa_cambio { get; set; }
        public int? id_moneda { get; set; }
        public string maquinafiscal { get; set; }
        public string maquinafiscal_devolucion { get; set; }
        public int? id_factura_fiscal { get; set; }
        public int? id_devolucion_fiscal { get; set; }
        public string usr_crea { get; set; }
        public DateTime? fec_crea { get; set; }
        public string usr_mod { get; set; }
        public DateTime? fec_mod { get; set; }
        public char enviado { get; set; }
        public int cantidad_nc { get; set; }

        public virtual Productos Producto { get; set; }
        public virtual DevolucionEncabezado DevolucionEncabezado { get; set; }
    }
}
