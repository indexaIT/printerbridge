﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterBridge.Library.Models
{
    [Table("cobrosfacturas", Schema = "postgres")]
    public class CobrosFacturas
    {
        [Key, Column("id_factura", Order = 0)]
        public int Id_Factura { get; set; }
        [Key, Column("id_tienda", Order = 1)]
        public int Id_Tienda { get; set; }
        [Key, Column("id_formacobro", Order = 2)]
        public int Id_FormaCobro { get; set; }
        [Column("id_caja")]
        public int Id_Caja { get; set; }
        [Column("id_cajero")]
        public int Id_Cajero { get; set; }
        [Column("id_cobrocaja")]
        public int Id_CobroCaja { get; set; }
        [Column("id_cuenta")]
        public int Id_Cuenta { get; set; }
        [Column("documento")]
        public string Documento { get; set; }
        [Column("id_tiendadoc")]
        public int? Id_TiendaDoc { get; set; }
        [Column("fechacobro")]
        public DateTime? FechaCobro { get; set; }
        [Column("montocobro")]
        public decimal MontoCobro { get; set; }
        [Column("id_moneda")]
        public int? Id_Moneda { get; set; }
        [Column("tasa_cambio")]
        public decimal? Tasa_Cambio { get; set; }
        public string usr_crea { get; set; }
        public DateTime? fec_crea { get; set; }
        public string usr_mod { get; set; }
        public DateTime? fec_mod { get; set; }
        public char enviado { get; set; }
    }
}
